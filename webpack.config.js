const path = require('path');


let config = {
    entry: ['./src/index.ts'],
    mode: "development",
    output: {
        path: path.join(__dirname, '/dist'),
        publicPath: '/',
        filename: 'index.js'
    },
    devtool: "source-map",

    resolve: {
        extensions: [".ts", ".js"],
    },

    // devServer: {
    //     contentBase: path.join(__dirname, 'dist'),
    //     compress: false,
    //     port: 8080,
    //     historyApiFallback: true,
    //     publicPath: '/',
    //     proxy: {
    //         '/api/highscore/*' : "http:localhost"
    //     },
    //     https: true
    // },

    module : {
        rules: [{
            test: /\.tsx?$/,
            loader: "ts-loader",
            exclude: /node_modules/
        }]
    },
    plugins: []

};

module.exports = config;
