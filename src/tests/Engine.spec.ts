import { expect } from "chai";
import "mocha";
import {Engine} from "../AECS/Engine";
import {TestWorld1, TestWorld2} from "./fixtures/Worlds";

describe('Engine Tests', function () {
    let engine: Engine;

    beforeEach(() => {
        engine = new Engine();
        let world1 = new TestWorld1(1);
        let world2 = new TestWorld2({});
        engine.addWorlds([world1, world2]);
    });

    it("should initialize as intended", () => {
        expect(engine["worldManager"]).to.not.equal(undefined);
        expect(Engine.Brain.worldManager).to.not.equal(undefined);
    });

    it("should have a static brain", () => {
        expect(Engine.Brain).to.not.equal(undefined);
    });

    it("should save all given Worlds", () => {
        expect(engine["worldManager"]["_worlds"].size).to.equal(2);
    });

    it("should throw when there is no active world set but you try to access the ecm", () => {
        expect(() => {
            let ecm = Engine.Brain.ecm;
            ecm.removeEntity(2)
        }).to.throw();
    });

    it("should be possible to set an active world", () => {
        engine.setActiveWorld(TestWorld1);
        expect(Engine.Brain.worldManager).to.not.equal(undefined);
    });

    it("has an instance of the eventManager", () => {
        engine.setActiveWorld(TestWorld1);
        expect(engine["eventManager"]).not.to.equal(undefined);
        expect(engine["worldManager"]["_activeWorld"]["eventManager"]).not.to.equal(undefined);
    });

    it("should throw when not initialized", () => {
        engine.setActiveWorld(TestWorld1);
        expect(() => {
            engine.update();
        }).to.throw();
        expect(() => {
            engine.start(60);
        }).to.throw();

        engine.init();
        expect(() => {
            engine.update();
        }).to.not.throw();
    });
});
