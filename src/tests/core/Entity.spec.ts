import { expect } from "chai";
import "mocha";
import {Entity} from "../../AECS/core/Entity";
import {Component} from "../../AECS/core/Component";
import {TransformComponent} from "../fixtures/Components";

class MyEntity extends Entity {
    constructor(id, components) {
        super(id, components);
    }
}

class AComponent extends Component {}
class BComponent extends Component {}

describe("Entity Tests", () => {
    let entity;
    beforeEach(() => {
         entity = new MyEntity("ASD", [new AComponent(), new BComponent()]);
    });

    it("should not accept empty/null/undefined id and be only settable once", () => {
        expect(() => {entity.id = ""}).to.throw();
        expect(() => {entity.id = 22}).to.throw();
        expect(entity.id).to.equal("ASD");
    });

    it("should not die when working with components", () => {
        expect(entity.components).length(2);
    });

    it("should get be injectable to have new components", () => {
        entity["addComponent"](TransformComponent);
        expect(entity.components).length(3);
    });

    it("should throw when you add the same type twice", () => {
        entity["addComponent"](TransformComponent);
        expect(() => {
            entity["addComponent"](TransformComponent);
        }).to.throw();
    });

    it("should be possible to remove a component", () => {
        entity["removeComponent"](AComponent);
        expect(entity.components).length(1);
    });

    it("should have temporary components for the Manager to register later", () => {
        expect(entity["_componentsToBeRegistered"]).length(2);
        expect(entity["_componentsToBeRegistered"][0].constructor.name).to.equal("AComponent");
    });

});