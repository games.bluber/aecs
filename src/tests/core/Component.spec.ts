import { expect } from "chai";
import "mocha";

import { Component} from "../../AECS/core/Component";

class MyComponent extends Component {
    x = 0;
    y = 0;
}

describe("Component Tests", () => {

   it("applyData() shall only set values which are actually declared inside the class", () => {
       let instance = new MyComponent();

       expect(instance.x).to.equal(0);
       expect(instance.y).to.equal(0);

       let obj = {
           x: 2,
           y: 2,
           z: 20
       };

       instance.applyData(obj);

       expect(instance.x).to.equal(2);
       expect(instance.y).to.equal(2);
       expect(instance["z"]).to.equal(undefined);
   });

});