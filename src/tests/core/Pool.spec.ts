import { expect } from "chai";
import "mocha";
import {Pool} from "../../AECS/core/Pool";
import {TestEntity1} from "../fixtures/Entities";
import {EntityComponentManager} from "../..";
import {TestPrefab} from "../fixtures/Prefabs";

describe("Pool Tests", () => {
    let pool: Pool<TestEntity1>;
    let ecm: EntityComponentManager;

    beforeEach(() => {
        ecm = new EntityComponentManager();
        pool = new Pool<TestEntity1>(new TestPrefab(TestEntity1), 2);
        pool["setEcm"](ecm);
    });

    it("should init the pool when calling .init()", () => {
        expect(pool["reserveList"].length).to.equal(0);
        expect(pool["activeList"].length).to.equal(0);
        pool["init"](10);
        expect(pool["reserveList"].length).to.equal(10);
    });

    it("initialized entities should be inactive", () => {
        pool["init"](10);
        let entity = pool["reserveList"][0];
        expect(entity.isActive).to.equal(false);
    });

    it("should set an entity active when asking the pool for a new entity", () => {
        pool["init"](10);
        let entityItem = pool.getEntity();
        expect(entityItem.entity.isActive).to.equal(true);
        expect(pool["reserveList"].length).to.equal(9);
        expect(pool["activeList"].length).to.equal(1);
    });

    it("should be possible to return an entity to the pool", () => {
        pool["init"](10);
        let entityItem = pool.getEntity();
        pool.returnEntity(entityItem.entity);
        expect(pool["reserveList"].length).to.equal(10);
        expect(pool["activeList"].length).to.equal(0);
    });

    it("should grow when all reserves are depleted", () => {
        pool["init"](1);
        expect(pool["reserveList"].length).to.equal(1);
        pool.getEntity();
        expect(pool["reserveList"].length).to.equal(0);
        pool.getEntity();
        expect(pool["activeList"].length).to.equal(2);
        expect(pool["reserveList"].length).to.equal(1);

    });
});
