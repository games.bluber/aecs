import { expect } from "chai";
import "mocha";
import {TestEntity1} from "../fixtures/Entities";
import {System} from "../../AECS/core/System";
import {EntityComponentManager} from "../../AECS/manager/EntityComponent.manager";
import {ComponentItem} from "../../AECS/manager/Component.manager";
import {EntityItem} from "../..";

class MySystem extends System {
    constructor(isActive: boolean) {
        super(isActive);
    }

    run(delta: number): void {

    }

    init(): void {
    }

}

describe("System Tests", () => {
    let system: MySystem;

    beforeEach(() => {
        system = new MySystem(true);
        system.setEcm(new EntityComponentManager());
    });

    it("should create and be extendable", ()=> {
        expect(new MySystem(true)).to.be.instanceof(System);
        expect(new MySystem(false)).to.be.instanceof(MySystem);
    });

    it("should have a system query", () => {
        expect(system["systemQuery"]).to.be.not.null;
    });

    it("can be set active and inactive", () => {
        expect(system.isActive).to.equal(true);
        system.pause();
        expect(system.isActive).to.equal(false);
        system.stop();
        expect(system.isActive).to.equal(false);
    });

    it("should have a run method", () => {
        expect(system.run).to.be.an("function");
    });

    it("has the ecm ready when doing its init", () => {
        system.setEcm(new EntityComponentManager());
        system.init();

        expect(system["ecm"]).not.equal(undefined);
    });

    // it("should have access to Engine.Brain", () => {
    //     expect(system["brain"]).to.not.equal(undefined);
    //     expect(system["brain"].storeManager).to.not.equal(undefined);
    // });

});
