
import {AComponent, BComponent, TransformComponent} from "./Components";
import {Entity} from "../../AECS/core/Entity";

export class TestEntity1 extends Entity {
    deletion: boolean = false;

    constructor(id?) {
        super(id ? id : 0, [
            new AComponent(),
            new BComponent()
        ]);
    }

    public onRemove(): void {
        super.onRemove();
        this.deletion = true;
    }
}

export class TestEntity2 extends Entity {
    constructor() {
        super(1, [
            new AComponent(),
            new TransformComponent()
        ]);
    }
}
