import {World} from "../../AECS/core/World";
import {TestSystem1} from "./Systems";

export class TestWorld1 extends World<number> {
    constructor(props: number) {
        super(props);
    }

    public init(): void {
        this.initSystems([
            new TestSystem1()
        ]);
    }

    update(delta: number): void {

    }
}

export class TestWorld2<T> extends World<T> {
    constructor(props: T) {
        super(props);
    }

    public init(): void {

    }

    update(delta: number): void {

    }
}
