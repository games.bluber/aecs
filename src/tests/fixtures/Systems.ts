
import {TestEntity1} from "./Entities";
import {System} from "../../AECS/core/System";
import {ComponentItem} from "../../AECS/manager/Component.manager";
import {EntityItem} from "../..";

export class TestSystem1 extends System {
    // public testEntites = [];
    // public testComponents = [];
    public hasBeenCalled = false;

    constructor() {
        super(true);
    }

    public run(delta: number): void {

    }

    public init(): void {
        this.listenOnEvent("someEvent", () => {this.hasBeenCalled = true})
    }


}


export class TestSystem2 extends System {
    // public testEntites = [];
    // public testComponents = [];
    public hasBeenCalled = false;

    constructor() {
        super(true);
    }

    public run(delta: number): void {
    }

    public init(): void {
        this.listenOnEvent("someEvent", () => {this.hasBeenCalled = true});
    }
}
