import {Component} from "../../AECS/core/Component";


export class TransformComponent extends Component {
    x = 0;
    y = 0;
    angle = 0;

    deletion: boolean = false;

    onRemove(): void {
        super.onRemove();
        this.deletion = true;
    }
}

export class AComponent extends Component {}
export class BComponent extends Component {}