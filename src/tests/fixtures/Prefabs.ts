import {Prefab} from "../../AECS/core/Prefab";
import {TestEntity1} from "./Entities";

let idHelper = 0;

export class TestPrefab extends Prefab<TestEntity1> {
    protected init(): void {

    }

    instantiate(): TestEntity1 {
        return new TestEntity1(idHelper++);
    }

}
