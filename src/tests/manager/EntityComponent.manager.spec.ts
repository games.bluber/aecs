import { expect } from "chai";
import "mocha";
import {EntityComponentManager} from "../../AECS/manager/EntityComponent.manager";
import {TestEntity1, TestEntity2} from "../fixtures/Entities";
import {AComponent, TransformComponent} from "../fixtures/Components";

describe("EntityComponentManager Tests", () => {
    let entityComponentManager: EntityComponentManager;

    const entities = "entities";
    const entityManagers = "entityManagers";
    const componentManagers = "componentManagers";

    beforeEach(() => {
        entityComponentManager = new EntityComponentManager();
    });

    it("should initialize as intended", () => {
        expect(entityComponentManager[entities].size).to.equal(0);
        expect(entityComponentManager[entityManagers].size).to.equal(0);
        expect(entityComponentManager[componentManagers].size).to.equal(0);
    });

    it("should add an entity to the system and register all components", () => {
        let entity = new TestEntity1();
        entityComponentManager.addEntity(entity);

        expect(entityComponentManager[entities].size).to.equal(1);
        expect(entityComponentManager[entityManagers].size).to.equal(1);
        expect(entityComponentManager[componentManagers].size).to.equal(2);
    });

    it("should remove all components from the entity during registration", () => {
        let entity = new TestEntity1();
        entityComponentManager.addEntity(entity);
        expect(entity["_componentsToBeRegistered"]).length(0);
    });

    it("should remove an entity and de-register all components", () => {
        let entity = new TestEntity1();
        entityComponentManager.addEntity(entity);
        entityComponentManager.removeEntity(entity.id);

        expect(entityComponentManager[entities].size).to.equal(0);
        expect(entityComponentManager[entityManagers].size).to.equal(1);
        expect(entityComponentManager[componentManagers].size).to.equal(2);
    });

    it("should get an Entity with all its components", () => {
        let entity1 = new TestEntity1();
        let entity2 = new TestEntity2();

        entityComponentManager.addEntity(entity1);
        entityComponentManager.addEntity(entity2);

        let returnValue = entityComponentManager.getEntity(entity2.id);

        expect(Object.keys(returnValue.components)).length(2);
        expect(returnValue.entity.id).to.equal(1);
    });

    it("should get all components of the same type", () => {
        let entity1 = new TestEntity1();
        let entity2 = new TestEntity2();

        entityComponentManager.addEntity(entity1);
        entityComponentManager.addEntity(entity2);

        let components = entityComponentManager.getComponents<TransformComponent>(TransformComponent);
        expect(components).length(1);
        expect(components[0].value.x).to.equal(0);
    });

    it("should get all entities of a same type at once", () => {
        let entity1 = new TestEntity1();
        let entity2 = new TestEntity2();

        entityComponentManager.addEntity(entity1);
        entityComponentManager.addEntity(entity2);

        let entities = entityComponentManager.getEntities(TestEntity1);
        expect(entities).length(1);
    });

    it("should throw then the same entity is added twice", () => {
        let entity = new TestEntity1();

        entityComponentManager.addEntity(entity);
        expect(() => {
            entityComponentManager.addEntity(entity);
        }).to.throw();
    });

    it("should be possible to set an entity inactive and all its components will be set inactive aswell", () => {
        let entity = new TestEntity1();

        entityComponentManager.addEntity(entity);
        entityComponentManager.setEntityActive(false, entity.id);
        expect(entity.isActive).to.equal(false);
        let instance = entityComponentManager.getEntity(entity.id);
        expect(instance.components.AComponent["isActive"]).to.equal(false);

        entityComponentManager.setEntityActive(true, entity.id);
        expect(instance.components.AComponent["isActive"]).to.equal(true);
    });
});
