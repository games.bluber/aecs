import { expect } from "chai";
import "mocha";
import {EventManager} from "../../AECS/manager/Event.manager";
import {TestSystem1, TestSystem2} from "../fixtures/Systems";

describe("Event Manager Tests", () => {
    let eventManager: EventManager;
    let testSystem1: TestSystem1;
    let testSystem2: TestSystem2;

    beforeEach(() => {
        eventManager = new EventManager();
        testSystem1 = new TestSystem1();
        testSystem1.setEventManager(eventManager);
        testSystem2 = new TestSystem2();
        testSystem2.setEventManager(eventManager);
    });

    it(".emit() should push events to the futureQueue for the next tick", () => {
        eventManager.emit("someEvent", {someData: 42});
        eventManager.emit("someEvent", {someData: 43});

        expect(eventManager["futureQueue"].size).to.equal(1);
        expect(eventManager["futureQueue"].get("someEvent").length).to.equal(2);
    });

    it("calling internal .endTick() puts data into the eventQueue und clears the futureQueue", () => {
        eventManager.emit("someEvent", {someData: 42});
        eventManager.emit("someEvent", {someData: 43});
        eventManager["endTick"]();

        expect(eventManager["futureQueue"].size).to.equal(0);
        expect(eventManager["eventQueue"].size).to.equal(1);
        expect(eventManager["eventQueue"].get("someEvent").length).to.equal(2);
    });

    it("systems need to have access to the eventManager", () => {
        expect(testSystem1["eventManager"]).to.not.equal(undefined);
    });

    it("registering the same system twice for the same event should result into an error", () => {
        eventManager.registerListener("someEvent", TestSystem1.name, () => {});
        expect(() => {
            eventManager.registerListener("someEvent", TestSystem1.name, () => {});
        }).to.throw();
        expect(() => {
            eventManager.registerListener("someEvent2", TestSystem1.name, () => {});
        }).to.not.throw();
    });

    it("multiple systems can listen to the same event", () => {
        eventManager.registerListener("someEvent", TestSystem1.name, () => {});
        expect(() => {
            eventManager.registerListener("someEvent", TestSystem2.name, () => {});
        }).to.not.throw();
        expect(eventManager["listeners"].size).to.equal(1);
        expect(Object.keys(eventManager["listeners"].get("someEvent")).length).to.equal(2);
    });
});
