import { expect } from "chai";
import "mocha";
import {PoolManager} from "../../AECS/manager/Pool.manager";
import {EntityComponentManager} from "../..";
import {Pool} from "../../AECS/core/Pool";
import {TestEntity1} from "../fixtures/Entities";
import {TestPrefab} from "../fixtures/Prefabs";

describe("Pool Manager Tests", () => {
    let poolManager: PoolManager;
    let ecm: EntityComponentManager;

    beforeEach(() => {
        poolManager = new PoolManager();
        ecm = new EntityComponentManager();
        poolManager["setEcm"](ecm);
    });

    it("should be possible to add a new pool to the manager", () => {
        let pool = new Pool(new TestPrefab(TestEntity1), 10);
        poolManager.addPool(pool, 10);

        expect(poolManager["pools"].size).to.equal(1);
        expect(pool["reserveList"].length).to.equal(10);
    });

    it("should throw when the same type of pool will be added more than once", () => {
        let pool = new Pool(new TestPrefab(TestEntity1), 10);
        poolManager.addPool(pool, 1);
        expect(() => {
            poolManager.addPool(pool, 1)
        }).to.throw();
    });

    it("should be possible to get a pool from the manager", () => {
        let pool = new Pool(new TestPrefab(TestEntity1), 10);
        poolManager.addPool(pool, 1);

        let fetchedPool = poolManager.getPool(TestEntity1);
        expect(fetchedPool).to.not.equal(undefined);
    });

    it("should throw when now entityComponentManager has been set", () => {
        let pm = new PoolManager();
        let pool = new Pool(new TestPrefab(TestEntity1), 10);
        expect(() => {
            pm.addPool(pool, 1);
        }).to.throw();
    })
});
