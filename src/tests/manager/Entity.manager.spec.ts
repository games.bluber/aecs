import { expect } from "chai";
import "mocha";
import {EntityManager} from "../../AECS/manager/Entity.manager";
import {TestEntity1, TestEntity2} from "../fixtures/Entities";

describe("EntityManager Tests", () => {
    let entityManager: EntityManager<any>;

    beforeEach(() => {
        entityManager = new EntityManager(TestEntity1.name);
    });

    it("should be created as intended", () => {
        expect(entityManager["idToEntityMap"].size).to.equal(0);
        expect(entityManager.size).to.equal(0);
    });

    it("should successfully add an entity to the manager", () => {
        let entity = new TestEntity1();
        let components = {};
        while(entity["_componentsToBeRegistered"].length) {
            let component = entity["_componentsToBeRegistered"].pop();
            components[component.constructor.name] = component;
        }

        entityManager.addEntity({
            entity: entity,
            components: components
        });

        expect(entityManager.size).to.equal(1);
    });

    it("should throw when the same entity is added twice", () => {
        let entity = new TestEntity1();
        let components = {};
        while(entity["_componentsToBeRegistered"].length) {
            let component = entity["_componentsToBeRegistered"].pop();
            components[component.constructor.name] = component;
        }

        entityManager.addEntity({
            entity: entity,
            components: components
        });

        expect(() => {
            entityManager.addEntity({
                entity: entity,
                components: components
            });
        }).to.throw();
    });

    it("should remove an entity from the manager", () => {
        let entity = new TestEntity1();
        let components = {};
        while(entity["_componentsToBeRegistered"].length) {
            let component = entity["_componentsToBeRegistered"].pop();
            components[component.constructor.name] = component;
        }

        entityManager.addEntity({
            entity: entity,
            components: components
        });

        entityManager.removeEntity(entity.id);

        expect(entityManager.size).to.equal(0);
    });

    it("should throw when two different types are added to the manager", () => {
        let entity = new TestEntity1();
        let components = {};
        while(entity["_componentsToBeRegistered"].length) {
            let component = entity["_componentsToBeRegistered"].pop();
            components[component.constructor.name] = component;
        }

        let entity2 = new TestEntity2();
        components = {};
        while(entity2["_componentsToBeRegistered"].length) {
            let component = entity2["_componentsToBeRegistered"].pop();
            components[component.constructor.name] = component;
        }

        expect(() => {
            entityManager.addEntity({
                entity: entity2,
                components: components
            });
        }).to.throw();
    });

    it("should get an entity", () => {
        let entity = new TestEntity1();
        let components = {};
        while(entity["_componentsToBeRegistered"].length) {
            let component = entity["_componentsToBeRegistered"].pop();
            components[component.constructor.name] = component;
        }

        entityManager.addEntity({
            entity: entity,
            components: components
        });

        let returnValue = entityManager.getEntity(entity.id);
        expect(returnValue.entity.constructor.name).to.equal(TestEntity1.name);
    });

    it("should call onRemove before removing the entity", () => {
        let ent1 = new TestEntity1(1);
        let ent2 = new TestEntity1(2);
        let ent3 = new TestEntity1(3);
        let comps = {};

        entityManager.addEntity({
            entity: ent1,
            components: comps
        });

        entityManager.addEntity({
            entity: ent2,
            components: comps
        });

        entityManager.addEntity({
            entity: ent3,
            components: comps
        });

        entityManager.removeEntity(2);
        expect(ent1.deletion).to.equal(false);
        expect(ent2.deletion).to.equal(true);
        expect(ent3.deletion).to.equal(false);
    });
});