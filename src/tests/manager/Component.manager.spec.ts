import { expect } from "chai";
import "mocha";
import {ComponentManager} from "../../AECS/manager/Component.manager";
import {AComponent, TransformComponent} from "../fixtures/Components";

describe("ComponentManager Tests", () => {
    let componentManager;

    beforeEach(() => {
        componentManager = new ComponentManager(TransformComponent.name);
    });

    it("should manage components without memory leaks", () => {
        expect(componentManager.size).to.equal(0);

        let comp1 = new TransformComponent();
        let comp2 = new TransformComponent();

        componentManager.addComponent(comp1, "hans");
        componentManager.addComponent(comp2, 22);

        expect(componentManager.size).to.equal(2);

        componentManager.removeComponent("hans");
        expect(componentManager.size).to.equal(1);

        let val = componentManager.getComponent(22);
        expect(val).to.not.equal(undefined);
    });

    it("should throw an error if you want to manage two types by one manager", () => {
        let comp3 = new AComponent();

        expect(() => {
            componentManager.addComponent(comp3, "asd");
        }).to.throw();
    });

    it("should call onRemove before removing the component", () => {
        let comp1 = new TransformComponent();
        let comp2 = new TransformComponent();
        let comp3 = new TransformComponent();

        componentManager.addComponent(comp1, 1);
        componentManager.addComponent(comp2, 2);
        componentManager.addComponent(comp3, 3);
        componentManager.removeComponent(2);

        expect(comp2.deletion).to.equal(true);
        expect(comp1.deletion).to.equal(false);
        expect(comp3.deletion).to.equal(false);
    });
});