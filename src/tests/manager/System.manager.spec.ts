import { expect } from "chai";
import "mocha";
import {SystemManager} from "../../AECS/manager/System.manager";
import {TestSystem1, TestSystem2} from "../fixtures/Systems";
import {TestEntity1} from "../fixtures/Entities";
import {EntityComponentManager} from "../../AECS/manager/EntityComponent.manager";
import {EventManager} from "../../AECS/manager/Event.manager";

describe("SystemManager Tests", () => {
    let sm;
    let eventManager: EventManager;

    beforeEach(() => {
        sm = new SystemManager(new EntityComponentManager(), {});
        eventManager = new EventManager();
        sm.setEventManager(eventManager);
    });

    it("should init the given systems into a map", () => {
        expect(sm["systems"].size).to.equal(0);
        sm.initSystems([new TestSystem1()]);
        expect(sm["systems"].size).to.equal(1);
    });

    // it("should save the queries for the systems", () => {
    //     let testSystem = new TestSystem1();
    //     sm.initSystems([testSystem]);
    //     let map = sm["systems"];
    //     let val = map.get(testSystem.constructor.name);
    //     expect(val.query.entities.length).to.equal(1);
    //     expect(val.query.entities).to.eql([TestEntity1]);
    // });

    // it("should inject the components and entities into the system by the given query", () => {
    //     let system = new TestSystem1();
    //     let ecm = sm["ecm"] as EntityComponentManager;
    //     ecm.addEntity(new TestEntity1());
    //
    //     sm.initSystems([system]);
    //     sm.callSystems(0);
    //
    //     expect(system.testEntites.length).to.equal(1);
    // });

    it("should invoke events in the same order as the systems are called, the eventManager beneath should have an empty eventQueue", () => {
        let system1 = new TestSystem1();
        let system2 = new TestSystem2();

        sm.initSystems([system1, system2]);
        eventManager.emit("someEvent", {});
        expect(eventManager["futureQueue"].size).to.equal(1); //thus two sm.callSystems are needed - events move from future to current tick
        sm.callSystems(0);
        sm.callSystems(0);

        expect(system1.hasBeenCalled).to.equal(true);
        expect(system2.hasBeenCalled).to.equal(true);
        expect(eventManager["eventQueue"].size).to.equal(0);
        expect(eventManager["futureQueue"].size).to.equal(0);
    });
});
