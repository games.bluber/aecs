import { expect } from "chai";
import "mocha";
import {StoreManager} from "../../AECS/manager/Store.manager";
import {TestStore} from "../fixtures/Store";

describe("StoreManager Tests", () => {
    let storeManager: StoreManager;

    beforeEach(() => {
        storeManager = new StoreManager();
        let store = new TestStore();
        storeManager.setStore(store);
    });

    it("should be possible to set a store", () => {
          expect(storeManager["stores"].size).to.equal(1);
    });

    it("should be possible to get a store with its corresponding type", () => {
        let store = storeManager.getStore<TestStore>(TestStore);
        store.order++;
        expect(store.order).to.equal(1);
    });
});
