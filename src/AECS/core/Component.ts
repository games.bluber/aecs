export abstract class Component {

    private _isActive: boolean = true;

    public get isActive(): boolean {
        return this._isActive;
    }

    public setActive(isActive: boolean): void {
        this._isActive = isActive;
        isActive ? this.onActivate() : this.onDeactivate();
    }

    public applyData(data: any): void {
        for(let k in data) {
            if(Object.keys(this).indexOf(k) != -1) {
                this[k] = data[k];
            }
        }
    }

    public onRemove(): void {}

    public onActivate(): void {}

    public onDeactivate(): void {}

}
