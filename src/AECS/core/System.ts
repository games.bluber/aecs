
import {EventManager} from "../manager/Event.manager";
import {Component} from "./Component";
import {Entity} from "./Entity";
import {EntityComponentManager} from "../manager/EntityComponent.manager";
import {ComponentItem} from "../manager/Component.manager";
import {EntityItem} from "../manager/Entity.manager";
import {Brain} from "../Brain";
import {Engine} from "../Engine";
import {StoreManager} from "../manager/Store.manager";
import {PoolManager} from "../manager/Pool.manager";
import {PrefabManager} from "../manager/Prefab.manager";

export interface SystemQuery {
    entities?: typeof Entity[];
    components?: typeof Component[];
}

/**
 * QUERIES FOR COMPONENTS CURRENTLY MAYBE DONT MAKE SENSE
 * - e.g. query 2 components [c1, c2] it will return ALL instances of c1 and c2 not only entities which have both c1, and c2
 * -> meaning c1.count != c2.count in worst case
 */
export abstract class System {
    protected ecm: EntityComponentManager;
    // protected brain: Brain = Engine.Brain;
    protected worldProperties: any;
    protected storeManager: StoreManager = Engine.Brain.storeManager;
    protected poolManager: PoolManager = Engine.Brain.poolManager;
    protected prefabManager: PrefabManager = Engine.Brain.prefabManager;

    private _isActive: boolean;
    private eventManager: EventManager;
    private eventList: string[];
    // private systemQuery: SystemQuery;

    public constructor(isActive: boolean) {
        this._isActive = isActive;
        // this.systemQuery = systemQuery;
        this.eventList = [];
    }

    private processEvent(): void {
        for(let event of this.eventList) {
            this.eventManager["processEvent"](event, this.constructor.name);
        }
    }

    protected listenOnEvent(eventName: string, callback: Function): void {
        this.eventList.push(eventName);
        this.eventManager.registerListener(eventName, this.constructor.name, callback);
    }

    protected submitEvent(eventName: string, eventData: any): void {
        this.eventManager.emit(eventName, eventData);
    }

    public setEcm(ecm: EntityComponentManager): void {
        this.ecm = ecm;
    }

    public setEventManager(em: EventManager): void {
        this.eventManager = em;
    }

    public setWorldProperties(props: any) : void {
        this.worldProperties = props;
    }

    public pause(): void {
        this._isActive = false;
    }

    public unpause(): void {
        this._isActive = true;
    }

    public get isActive(): boolean {
        return this._isActive;
    }

    public stop(): void {
        this._isActive = false;
    }

    public abstract init(): void;
    // public abstract run(delta: number, entities: Array<EntityItem<any>>, components: Array<ComponentItem<any>>): void;
    public abstract run(delta: number): void;

    public onShutdown(): void {}
}
