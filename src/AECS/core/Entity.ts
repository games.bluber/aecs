import {Component} from "./Component";

export abstract class Entity {
    private _id: number | string;
    private _components: Array<string> = [];
    private _componentsToBeRegistered: Array<Component> = [];
    private _isActive: boolean = true;

    public constructor(id: number | string, components: Array<Component>) {
        this.id = id;
        this._componentsToBeRegistered = components;
        for(let component of components) {
            this._components.push(component.constructor.name);
        }
    }

    public get isActive(): boolean {
        return this._isActive;
    }

    public setActive(isActive: boolean): void {
        this._isActive = isActive;
        isActive ? this.onActivate() : this.onDeactivate();
    }

    public set id(id: number | string) {
        if(id === undefined || id === null || id === "") throw new Error("You need to provide an ID to set for Entity of type: "+this.constructor.name);

        if(this._id !== null && this._id !== undefined) throw new Error("Entity ID is already set: "+this._id);

        this._id = id;
    }

    public get id() {
        if(this._id === null) throw new Error("Cannot retrieve and ID when is null");

        return this._id;
    }

    public get components(): Array<string>{
        return this._components;
    }

    private removeComponent(cType: typeof Component): void {
        let i = this._components.indexOf(cType.name);
        if(i != -1) {
            this._components.splice(i, 1);
        }
    }

    private addComponent(cType: typeof Component): void {
        let i = this._components.indexOf(cType.constructor.name);
        if(i != -1) throw  new Error("you cannot add the same component twice: "+cType.constructor.name);
        this._components.push(cType.constructor.name);
    }

    public onRemove(): void {}

    public onActivate(): void {}

    public onDeactivate(): void {}
}
