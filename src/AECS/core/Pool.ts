import {Entity} from "./Entity";
import {EntityComponentManager, EntityItem} from "../..";
import {Prefab} from "./Prefab";

export class Pool<T extends Entity> {
    private activeList: T[];
    private reserveList: T[];
    private prefab: Prefab<T>;
    private growth: number;

    private ecm: EntityComponentManager;

    constructor(prefab: Prefab<T>, growth: number) {
        this.growth = growth;
        this.activeList = [];
        this.reserveList = [];
        this.prefab = prefab;
    }

    private setEcm(ecm: EntityComponentManager): void {
        this.ecm = ecm;
    }

    private init(amount: number, ): void {
        for(let i = 0; i < amount; i++) {
            let entity = this.prefab.instantiate();
            this.ecm.addEntity(entity);
            this.ecm.setEntityActive(false, entity.id);
            this.reserveList.push(entity);
        }
    }

    public getEntity(): EntityItem<T> {
        if(this.reserveList.length == 0) {
            this.init(this.growth);
        }

        const entity = this.reserveList.pop();
        this.ecm.setEntityActive(true, entity.id);
        this.activeList.push(entity);
        return this.ecm.getEntity<T>(entity.id);
    }

    public returnEntity(entity: T): void {
        const index = this.activeList.indexOf(entity);
        if(index >= 0) {
            this.activeList.splice(index, 1);
            this.reserveList.push(entity);
            this.ecm.setEntityActive(false, entity.id);
        }
    }
}
