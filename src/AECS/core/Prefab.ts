import {Entity} from "./Entity";

export abstract class Prefab<T extends Entity> {
    private entityType: typeof Entity;

    constructor(entityType: typeof Entity) {
        this.entityType = entityType;
        this.init();
    }

    protected abstract init(): void;
    public abstract instantiate(): T;
}
