
import {System} from "./System";
import {SystemManager} from "../manager/System.manager";
import {EventManager} from "../manager/Event.manager";
import {EntityComponentManager} from "../manager/EntityComponent.manager";
import {PrefabManager} from "../manager/Prefab.manager";
import {Engine} from "../Engine";
import {Prefab} from "./Prefab";

export abstract class World<T> {
    public ecm: EntityComponentManager;
    public readonly properties: T;
    private eventManager: EventManager;
    private prefabManager: PrefabManager = Engine.Brain.prefabManager;
    public systemManager: SystemManager;

    constructor(properties: T) {
        this.ecm = new EntityComponentManager();
        this.properties = properties;
        this.systemManager = new SystemManager(this.ecm, this.properties);
    }

    protected initSystems(systems: Array<System>): void {
        this.systemManager.initSystems(systems);
    }

    protected initPrefabs(prefabs: Prefab<any>[]): void {
        for(let prefab of prefabs) {
            this.prefabManager.addPrefab(prefab);
        }
    }

    protected callSystems(delta: number): void {
        this.systemManager.callSystems(delta);
    }

    public shutdown(): void {
        //todo this.ecm.clear();
        this.systemManager.stopSystems();
    }

    public setEventManager(em: EventManager): void {
        this.eventManager = em;
        this.systemManager.setEventManager(em);
    }

    abstract init(): void;

    abstract update(delta: number): void;

}
