import {EntityComponentManager} from "./manager/EntityComponent.manager";
import {WorldManager} from "./manager/World.manager";
import {StoreManager} from "./manager/Store.manager";
import {PoolManager} from "./manager/Pool.manager";
import {PrefabManager} from "./manager/Prefab.manager";

export class Brain {
    private _entityComponentManager: EntityComponentManager;
    private _worldManager: WorldManager; //@ experimental - maybe not needed
    private _storeManager: StoreManager = new StoreManager();
    private _poolManager: PoolManager = new PoolManager();
    private _prefabManager: PrefabManager = new PrefabManager();

    public get prefabManager(): PrefabManager {
        return this._prefabManager;
    }

    public get poolManager(): PoolManager {
        return this._poolManager;
    }

    public get storeManager(): StoreManager {
        return this._storeManager;
    }

    public get ecm(): EntityComponentManager {
        if(!this._entityComponentManager) throw new Error("EntityComponentManager not yet set! Ensure you are using the init method of the World when using Brain's ecm");
        return this._entityComponentManager;
    }

    public set ecm(ecm: EntityComponentManager) {
        this._entityComponentManager = ecm;
        this._poolManager["setEcm"](ecm);
    }

    public get worldManager(): WorldManager {
        if(!this._worldManager) throw new Error("Iam too lazy right now to implement a proper message");
        return this._worldManager;
    }

    public set worldManager(worldManager: WorldManager) {
        this._worldManager = worldManager;
    }
}
