import {ComponentItem, ComponentManager} from "./Component.manager";
import {EntityItem, EntityManager} from "./Entity.manager";
import {Entity} from "../core/Entity";
import {Component} from "../core/Component";
import {Prefab} from "../core/Prefab";
import {SystemQuery} from "../..";

export class EntityComponentManager {
    //string = type
    private componentManagers: Map<string, ComponentManager<any>> = new Map<string, ComponentManager<any>>();
    //string = type
    private entityManagers: Map<string, EntityManager<any>> = new Map<string, EntityManager<any>>();

    private entities: Map<string | number, string> = new Map<string | number, string>();

    public instantiatePrefab<T extends Entity>(prefab: Prefab<T>): void {
        this.addEntity(prefab.instantiate());
    }

    public addEntity(entity: Entity): void {
        if(this.entities.has(entity.id)) {
            throw new Error("Entity with the same ID is already added! ID: "+entity.id+ " TYPE: "+entity.constructor.name);
        } else {
            let components = [];
            while(entity["_componentsToBeRegistered"].length) {
                components.push(entity["_componentsToBeRegistered"].pop());
            }
            let componentReferences = {};
            for(let component of components) {
                if(!this.componentManagers.has(component.constructor.name)) this.componentManagers.set(component.constructor.name, new ComponentManager<any>(component.constructor.name));
                let compManager = this.componentManagers.get(component.constructor.name);
                compManager.addComponent(component, entity.id);
                componentReferences[component.constructor.name] = compManager.getComponent(entity.id);
            }
            let entityItem = {
                entity: entity,
                components: componentReferences
            };
            if(!this.entityManagers.has(entity.constructor.name)) this.entityManagers.set(entity.constructor.name, new EntityManager<any>(entity.constructor.name));
            this.entityManagers.get(entity.constructor.name).addEntity(entityItem);

            this.entities.set(entity.id, entity.constructor.name);
        }
    }

    public removeEntity(id: number | string): void {
        if(this.entities.has(id)) {
            let entityType = this.entities.get(id);
            let entManager = this.entityManagers.get(entityType);
            let entity = entManager.getEntity(id).entity;
            entManager.removeEntity(id);

            for(let cTypes of entity.components) {
                this.componentManagers.get(cTypes).removeComponent(entity.id);
            }
            this.entities.delete(entity.id);
        }
    }

    public getEntity<T extends Entity>(id: number | string): EntityItem<T> {
        let entity = this.entities.get(id);

        const entityManager = this.entityManagers.get(entity);
        let item = entityManager.getEntity(id);

        return {
            components: item.components,
            entity: item.entity
        }
    }

    public getComponents<T extends Component>(componentType: typeof Component): Array<ComponentItem<T>> {
        if(this.componentManagers.has(componentType.name))
            return this.componentManagers.get(componentType.name).getAllComponents();
    }

    public getComponent<T extends Component>(componentType: typeof Component, id: number | string): T {
        if(this.componentManagers.has(componentType.name))
            return this.componentManagers.get(componentType.name).getComponent(id);
    }

    //todo refactor so that you dont need a loop :)
    public getEntities<T extends Entity>(entityType: typeof Entity): Array<EntityItem<T>> {
        if(this.entityManagers.has(entityType.name))
            return this.entityManagers.get(entityType.name).getAllEntities();
    }

    public setEntityActive(isActive: boolean, id: string | number): void {
        let entity = this.getEntity(id);
        if(entity) {
            entity.entity.setActive(isActive);
            for(let k in entity.components) {
                entity.components[k]["setActive"](isActive);
            }
        }
    }
}
