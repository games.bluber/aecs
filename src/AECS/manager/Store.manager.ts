import {Store} from "../core/Store";

export class StoreManager {
    private stores: Map<string, Store>;

    constructor() {
        this.stores = new Map<string, Store>();
    }

    public getStore<T extends Store>(storeType: typeof Store): T {
        return this.stores.get(storeType.name) as T;
    }

    public setStore<T extends Store>(store: Store): void {
        this.stores.set(store.constructor.name, store);
    }

}
