import Func = Mocha.Func;

export class EventManager {
    private listeners: Map<string, {[caller_name: string]: Function}>;
    private eventQueue: Map<string, any[]>;
    private futureQueue: Map<string, any[]>;

    constructor() {
        this.listeners = new Map<string, {[caller_name: string]: Function}>();
        this.eventQueue = new Map<string, any[]>();
        this.futureQueue = new Map<string, any[]>();
    }

    public emit(eventName: string, data: any): void {
        if(this.futureQueue.has(eventName)) {
            this.futureQueue.get(eventName).push(data);
        } else {
            this.futureQueue.set(eventName, [data]);
        }
    }

    public registerListener(eventName: string, listener: string, callback: Function): void {
        if(this.listeners.has(eventName)) {
            let listeners = this.listeners.get(eventName);
            if(!listeners[listener]) { //listener X is not listening to the event so we can add him
                listeners[listener] = callback;
            } else {
                throw new Error(listener + " is already listening on " + eventName + " event");
            }
        } else {
            let listeners = {};
            listeners[listener] = callback;
            this.listeners.set(eventName, listeners);
        }
    }

    private processEvent(eventName: string, listener: string): void {
        if(this.eventQueue.has(eventName)) {
            let data = this.eventQueue.get(eventName);
            let listeners = this.listeners.get(eventName);
            if(listeners[listener]) {
                listeners[listener](data);
            }
        }
    }

    private endTick(): void {
        this.eventQueue = new Map<string, any[]>(this.futureQueue);
        this.futureQueue.clear();
    }
}
