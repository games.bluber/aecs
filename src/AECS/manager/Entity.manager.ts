import {Component} from "../core/Component";
import {Entity} from "../core/Entity";


export interface EntityItem<T> {
    components: {[type: string]: typeof Component};
    entity: T;
}

export class EntityManager<T extends Entity> {
    public readonly type: string;

    private idToEntityMap: Map<number | string, number>;
    private entities: Array<EntityItem<T>>;
    private _size: number;

    private allocatedSize: number = 1024;

    public constructor(type: string) {
        this.type = type;
        this.idToEntityMap = new Map<number|string, number>();
        this.entities = new Array<EntityItem<T>>(this.allocatedSize);
        this._size = 0;
    }

    public get size(): number {
        return this._size;
    }

    public addEntity(entityItem: EntityItem<T>): void {
        if(entityItem.entity.constructor.name !== this.type) throw new Error("This entity manager is only handling entities of type: "+this.type + " got: "+entityItem.entity.constructor.name);
        if(this.idToEntityMap.has(entityItem.entity.id)) throw new Error("Entity with ID: "+entityItem.entity.id + " already added");
        this.entities[this._size] = entityItem;
        this.idToEntityMap.set(entityItem.entity.id, this._size);
        this._size++;
    }

    public removeEntity(id: number | string): void {
        let positionOfDeletedItemInArray: number = this.idToEntityMap.get(id);

        if(positionOfDeletedItemInArray === this._size-1) {
            //last item
            this.entities[positionOfDeletedItemInArray].entity.onRemove();
            delete this.entities[positionOfDeletedItemInArray];
            this.idToEntityMap.delete(id);
            this._size--;
        } else {
            let lastItem = this.entities[this._size-1];
            delete this.entities[this._size-1];
            //override entity to be deleted with the last item
            this.entities[positionOfDeletedItemInArray].entity.onRemove();
            this.entities[positionOfDeletedItemInArray] = lastItem;
            this.idToEntityMap.set(lastItem.entity.id, positionOfDeletedItemInArray);
            this.idToEntityMap.delete(id);
            this._size--;
        }
    }

    public getEntity(id: number | string): EntityItem<T> {
        return this.entities[this.idToEntityMap.get(id)];
    }

    public getAllEntities(): Array<EntityItem<T>> {
        return this.entities.slice(0, this._size);
    }

}
