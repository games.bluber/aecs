import {

} from "../..";
import {EventManager} from "./Event.manager";
import {System, SystemQuery} from "../core/System";
import {EntityComponentManager} from "./EntityComponent.manager";
import {ComponentItem} from "./Component.manager";
import {EntityItem} from "./Entity.manager";
//
// interface SystemData {
//     system: System;
//     query: SystemQuery;
// }

export class SystemManager {
    private systems: Map<string, System>;
    private readonly ecm: EntityComponentManager;
    private readonly worldProperties: any;
    private eventManager: EventManager;

    constructor(ecm: EntityComponentManager, props: any) {
        this.systems = new Map<string, System>();
        this.ecm = ecm;
        this.worldProperties = props;
    }

    public setEventManager(em: EventManager): void {
        this.eventManager = em;
    }

    public initSystems(systems: Array<System>): void {
        for(let system of systems) {
            system.setEcm(this.ecm);
            system.setEventManager(this.eventManager);
            system.setWorldProperties(this.worldProperties);
            system.init();
            // this.systems.set(system.constructor.name, {
            //     system: system,
            //     query: system["systemQuery"]
            // });
            this.systems.set(system.constructor.name, system);
        }
    }

    public callSystems(delta: number): void {
        this.systems.forEach(((value, key) => {
            // let queryData = this.getQueryData(value.query);
            if(value.isActive) {
                value["processEvent"]();
                value.run(delta);
            }
        }));
        this.eventManager["endTick"](); // copy queued events to the eventQueue which will be worked on during the next tick
    }

    public stopSystems(): void {
        this.systems.forEach(value => {
            value.onShutdown();
            value.stop();
        });
    }

    // private getQueryData(queryData: SystemQuery): {entities: Array<EntityItem<any>>, components: Array<ComponentItem<any>>} {
    //     let entities = [];
    //     let components: ComponentItem<any>[] = [];
    //
    //     if(queryData.entities) for(let entityQuery of queryData.entities) {
    //         let entityData: EntityItem<any>[] = this.ecm.getEntities(entityQuery);
    //         if(entityData)
    //             entities = entities.concat(entityData);
    //     }
    //     if(queryData.components) for(let componentQuery of queryData.components) {
    //         let componentData: ComponentItem<any>[] = this.ecm.getComponents(componentQuery);
    //         if(componentData)
    //             components = components.concat(componentData);
    //     }
    //
    //     return {
    //         entities: entities,
    //         components: components
    //     };
    // }
}
