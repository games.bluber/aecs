import {Prefab} from "../core/Prefab";
import {EntityItem} from "./Entity.manager";
import {Entity} from "../..";

export class PrefabManager {
    private prefabs: Map<string, Prefab<any>>;

    constructor() {
        this.prefabs = new Map<string, Prefab<any>>();
    }

    public addPrefab(prefab: Prefab<any>): void {
        if(this.prefabs.has(prefab.constructor.name)) {
            throw new Error("You cannot add the same Prefab type to the manager multiple times: " + prefab.constructor.name);
        } else {
            this.prefabs.set(prefab.constructor.name, prefab);
        }
    }

    public getPrefab<T extends Prefab<any>>(prefabType: typeof Prefab): T {
        return this.prefabs.get(prefabType.name) as T;
    }

    public instantiate<T extends Entity>(prefabType: typeof Prefab): EntityItem<T> {
        if(this.prefabs.has(prefabType.name))
            return this.prefabs.get(prefabType.name).instantiate();
    }
}
