import {Component} from "../core/Component";
import {Entity} from "../core/Entity";


export interface ComponentItem<T> {
    value: T;
    id: number | string;
}

export class ComponentManager<T extends Component> {
    private entityToComponentMap: Map<number | string, number>;
    private components: Array<ComponentItem<T>>;
    private _size: number;

    private allocatedSize: number = 1024;

    public readonly type: string;

    public constructor(type: string, preAllocate?: typeof Entity) {
        this.type = type;
        this.entityToComponentMap = new Map<number|string, number>();
        this.components = new Array(this.allocatedSize);
        if(preAllocate) {
            for(let i = 0; i < this.allocatedSize; i++) {
                this.components[i] = {
                    value: {...preAllocate} as any,
                    id: 0
                };
            }
        }
        this._size = 0;
    }

    public get size(): number {
        return this._size
    }

    public addComponent(component: T, id: number | string): void {
        if(component.constructor.name !== this.type) throw new Error("This component manager is only handling components of type: "+this.type+" , got: " + component.constructor.name)
        this.components[this._size] = {value: component, id: id};
        this.entityToComponentMap.set(id, this.size);
        this._size++;
    }

    public removeComponent(id: number | string): void {
        let positionOfDeletedItemInArray: number = this.entityToComponentMap.get(id);
        let component = this.components[positionOfDeletedItemInArray];

        //check if position is last item in array
        if(positionOfDeletedItemInArray === this.size-1) {
            //last item
            this.components[positionOfDeletedItemInArray].value.onRemove();
            delete this.components[positionOfDeletedItemInArray];
            this.entityToComponentMap.delete(id);
            this._size--;
        } else {
            //item in between
            let lastItem: ComponentItem<any> = this.components[this.size-1];
            delete this.components[this.size-1];
            //override deleted component with last item in array
            this.components[positionOfDeletedItemInArray].value.onRemove();
            this.components[positionOfDeletedItemInArray] = lastItem;
            this.entityToComponentMap.set(lastItem.id, positionOfDeletedItemInArray);
            this.entityToComponentMap.delete(id);
            this._size--;
        }
    }

    public getComponent(id: number | string): T {
        return this.components[this.entityToComponentMap.get(id)].value;
    }

    public getAllComponents(): Array<ComponentItem<T>> {
        return this.components.slice(0, this.size);
    }
}