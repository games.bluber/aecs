import {Pool} from "../core/Pool";
import {Entity, EntityComponentManager} from "../..";

export class PoolManager {
    private pools: Map<string, Pool<any>>;
    private ecm: EntityComponentManager;

    constructor() {
        this.pools = new Map<string, Pool<any>>();
    }

    private setEcm(ecm: EntityComponentManager) {
        this.ecm = ecm;
    }

    public getPool<T extends Entity>(entityType: typeof Entity): Pool<T> {
        if(!this.ecm) throw new Error("PoolManager can only be accessed from within a system");
        if(this.pools.has(entityType.name)) {
            return this.pools.get(entityType.name);
        }
    }

    public addPool<T extends Entity>(pool: Pool<T>, initialPoolSize: number): void {
        if(!this.ecm) throw new Error("PoolManager can only be accessed from within a system");
        if (this.pools.has(pool["prefab"]["entityType"].name)) throw new Error("Pool with type <<"+pool["entityBlueprint"].name+">> already existing");

        pool["setEcm"](this.ecm);
        pool["init"](initialPoolSize);

        this.pools.set(pool["prefab"]["entityType"].name, pool);
    }
}
