import {World} from "../core/World";
import {EventManager} from "./Event.manager";

export class WorldManager {
    private readonly _worlds: Map<string, World<any>>;
    private _activeWorld: World<any>;

    constructor() {
        this._worlds = new Map<string, World<any>>();
    }

    public addWorlds(worlds: Array<World<any>>): void {
        for(let world of worlds) {
            this._worlds.set(world.constructor.name, world);
        }
    }

    public setActiveWorld<T>(worldType: typeof World<T>, eventManager: EventManager): void {
        if(this._worlds.has(worldType.name)) {
            this._activeWorld = this._worlds.get(worldType.name);
            this._activeWorld.setEventManager(eventManager);
        } else {
            throw new Error("Type: " + worldType.name + " is not existing");
        }
    }

    public getActiveWorld<T extends World<any>>(): T {
        return this._activeWorld as T;
    }
}
