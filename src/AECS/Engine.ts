import {Brain} from "./Brain";
import {WorldManager} from "./manager/World.manager";
import {World} from "./core/World";
import {EventManager} from "./manager/Event.manager";

export class Engine {
    public static Brain: Brain = new Brain();

    private readonly worldManager: WorldManager;
    private readonly eventManager: EventManager;
    private loop: any;
    private isInit: boolean;

    private lastUpdate: number;
    private deltaTime: number;

    public constructor() {
        this.worldManager = new WorldManager();
        this.eventManager = new EventManager();

        Engine.Brain.worldManager = this.worldManager;
    }

    public addWorlds<T>(worlds: Array<World<T>>): void {
        this.worldManager.addWorlds(worlds);
    }

    public setActiveWorld<T>(world: typeof World<T>): void {
        this.worldManager.setActiveWorld(world, this.eventManager);
        Engine.Brain.ecm = this.worldManager.getActiveWorld().ecm;
    }

    public init(): void {
        this.deltaTime = 0;
        this.lastUpdate = Date.now();
        this.worldManager.getActiveWorld().init();
        this.isInit = true;
    }

    public start(loopsPerSecond: number): void {
        if(!this.worldManager.getActiveWorld()) throw new Error("You need to set an active World before starting the Engine!");
        if(!this.isInit) throw new Error("Engine needs to be initialized - call .init() before calling start() or update()");
        this.loop = setInterval(() => {
            this.update(this.deltaTime);
        }, 1000/loopsPerSecond)
    }

    public update(deltaTime: number = -1): void {
        if(!this.worldManager.getActiveWorld()) throw new Error("You need to set an active World before starting the Engine!");
        if(!this.isInit) throw new Error("Engine needs to be initialized - call .init() before calling start() or update()");
        this.deltaTime = Date.now() - this.lastUpdate;
        this.lastUpdate = Date.now();
        this.worldManager.getActiveWorld().update(deltaTime == -1 ? this.deltaTime : deltaTime); //TODO
    }

    public sendEvent(eventName: string, eventData: any): void {
        this.eventManager.emit(eventName, eventData)
    }

}
